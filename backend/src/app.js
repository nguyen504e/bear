import { ApolloServer } from 'apollo-server'

import { createAuthScopeGetter } from './services/auth'
import { createClient } from './services/document_db'
import { getConfig } from './services/config'
import constance from './constance.json'
import resolvers from './modules/resolvers'
import typeDefs from './modules/type_defs'

export default async () => {
  const config = getConfig(constance.MODULE_NAME)
  const authScopeGetter = createAuthScopeGetter(config)
  const docDb = createClient(config)
  const context = ({ req }) => {
    return {
      config,
      docDb,
      authScope: authScopeGetter(req)
    }
  }
  const server = new ApolloServer({ resolvers, typeDefs, context })
  server.listen()
}
