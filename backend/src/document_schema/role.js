import { Schema } from 'mongoose'

import { PERMISSIONS } from './_permissions'

export default new Schema({
  name: String,
  permission: Array.from(PERMISSIONS.values())
})
