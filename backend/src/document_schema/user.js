import { Schema, ObjectId } from 'mongoose'

import { PERMISSIONS } from './_permissions'

export default new Schema({
  email: String,
  username: String,
  hashPwd: String,
  role: ObjectId,
  permission: Array.from(PERMISSIONS.values()),
  version: Number
})
