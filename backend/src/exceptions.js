import { ApolloError } from 'apollo-server'

class PropheticError extends ApolloError {
  constructor(name, message, code, properties) {
    super(message, code, properties)

    // Set the prototype explicitly.
    // https://stackoverflow.com/a/41102306
    Object.setPrototypeOf(this, SyntaxError.prototype)
    Object.defineProperty(this, 'name', { value: name })
  }
}

export class OptimisticLockError extends PropheticError {
  constructor(properties) {
    super(
      'OptimisticLockError',
      'Operation failed because another user has updated or deleted the record',
      'OPTIMISTIC_LOCK',
      properties
    )
  }
}

export class ExternalServiceError extends PropheticError {
  constructor(properties) {
    super('ExternalServiceError', 'External service response is not ok', 'EXTERNAL_SERVICE', properties)
  }
}

export class ServiceMissingError extends PropheticError {
  constructor(properties) {
    super('ServiceMissingError', 'Service is missing', 'SERVICE_MISSING', properties)
  }
}

export class InvalidDataError extends PropheticError {
  constructor(properties) {
    super('InvalidDataError', 'Data is invalid', 'INVALID_DATA', properties)
  }
}

export class EmptyDataError extends PropheticError {
  constructor(properties) {
    super('EmptyDataError', 'Data is empty', 'EMPTY_DATA', properties)
  }
}
export class SecurityTokenError extends PropheticError {
  constructor(properties) {
    super('SecurityTokenError', 'Security token is invalid', 'SECURITY_TOKEN', properties)
  }
}

export class AuthenticationRequiredError extends PropheticError {
  constructor(properties) {
    super('AuthenticationRequiredError', 'You must be logged in to do this', 'AUTH_REQUIRED', properties)
  }
}

export class UserNotFoundError extends PropheticError {
  constructor(properties) {
    super('UserNotFoundError', 'No user found', 'USER_NOT_FOUND', properties)
  }
}

export class RoleRequiredError extends PropheticError {
  constructor(properties) {
    super('RoleRequiredError', 'Role required not found', 'ROLE_REQUIRED', properties)
  }
}
