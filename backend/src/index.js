import app from './app'

;(async () => {
  if (__DEBUG__) {
    // eslint-disable-next-line
    debugger
    console.log('DEBUGGING...')
  }
  await app()
})()
