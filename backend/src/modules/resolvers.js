import { pick, values } from 'lodash'

import exeptions from '../exeptions'
import logger from '../services/logger'

export default {
  Query: {
    version: () => `${__VERSION__}_${__BUILD_TIME__}`,
    errors: () => values(exeptions).map(E => pick(new E(), ['name', 'message', 'extensions']))
  },

  Mutation: {
    log: () => logger.info('Manual log')
  }
}
