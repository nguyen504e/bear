import { JsonWebTokenError, verify } from 'jsonwebtoken'
import { every, get, includes, isObject, memoize } from 'lodash'

import assert from 'assert'

import { AuthenticationRequiredError, RoleRequiredError, SecurityTokenError } from '../exceptions'
import log from './logger'

export const createAuthScopeGetter = (config, opts = {}) => {
  const bearerRgx = /^Bearer /
  const secret = config.get('JWT_TOKEN_PUBLIC_KEY')

  return tokenWithBearer => {
    if (bearerRgx.test(tokenWithBearer)) {
      const token = tokenWithBearer.replace(bearerRgx, '')
      try {
        if (token) {
          return verify(token, secret, opts)
        }
        return null
      } catch (err) {
        if (err instanceof JsonWebTokenError) {
          log.error(err.message, token)
          throw new SecurityTokenError({ message: err.message })
        }
        throw err
      }
    }
    return null
  }
}

export const hasAuth = cb => (src, args, ctx, info) => {
  if (!isObject(ctx.authScope)) {
    throw new AuthenticationRequiredError()
  }

  return cb(src, args, ctx, info)
}

export const hasRoles = memoize(function() {
  const roles = Array.from(arguments)
  assert.notStrictEqual(roles.length, 0)

  return ctx => {
    const scope = get('authScope.scope')
    if (!every(roles, r => includes(scope, r))) {
      throw new RoleRequiredError()
    }
    return ctx
  }
})
