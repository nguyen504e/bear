import { memoize } from 'lodash'

import { cosmiconfig } from 'cosmiconfig'

export const getConfig = memoize((moduleName, configPath) => {
  const explorer = cosmiconfig(moduleName)
  const rawConfig = configPath ? explorer.loadSync(configPath) : explorer.searchSync()
  return new Map(rawConfig.entries())
})
