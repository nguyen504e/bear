import { memoize } from 'lodash'
import { mongoose } from 'mongoose'

export const createClient = memoize(_ =>
  mongoose.connect(_.get('DB_DOC_CONNECTION'), { useNewUrlParser: _.get('DB_DOC_NEW_URL') })
)
