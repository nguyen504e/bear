import fs from 'fs'

import { isEmpty } from 'lodash'
import pino from 'pino'
import pinoms from 'pino-multi-stream'

import { getConfig } from './config'
import constance from '../constance.json'

const config = getConfig(constance.MODULE_NAME)

const streams = []

const logFilePath = config.get('LOG_FILE_PATH')
const logFileLevel = config.get('LOG_FILE_LEVEL') || 'error'
const logConsoleLevel = config.get('LOG_CONSOLE_LEVEL') || 'error'
if (logFilePath || logFileLevel !== 'mute') {
  streams.push({ stream: fs.createWriteStream(logFilePath), level: logFileLevel })
}

if (logConsoleLevel !== 'mute') {
  streams.push({ stream: process.stdout, level: logConsoleLevel })
}

export default isEmpty(streams) ? pino() : pinoms({ streams })
