const path = require('path')
const nodeExternals = require('webpack-node-externals')
const webpack = require('webpack')
const WebpackBar = require('webpackbar')
const pkg = require('./package.json')
const { format } = require('date-fns')

module.exports = (env = {}) => {
  const plugins = [
    new webpack.DefinePlugin({
      __DEBUG__: JSON.stringify(!!env.debug),
      __DEV__: JSON.stringify(!!env.development),
      __BUILD_TIME__: JSON.stringify(format(new Date(), 'yyMMddHHmmss xx')),
      __VERSION__: JSON.stringify(pkg.version)
    })
  ]

  if (env.development) {
    const NodemonPlugin = require('nodemon-webpack-plugin')
    plugins.push(
      new NodemonPlugin({
        ignore: ['src', 'node_modules']
      })
    )
  }

  plugins.push(new WebpackBar({ profile: true }))

  return {
    target: 'node',
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, './dist')
    },
    externals: [nodeExternals()],
    plugins,
    stats: 'errors-only',
    module: {
      rules: [
        {
          test: /\.graphql$/i,
          use: 'raw-loader'
        }
      ]
    }
  }
}
